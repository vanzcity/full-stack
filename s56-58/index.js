function countLetter(letter, sentence) {
    let result = 0;
    let i = 0;

    if(letter.length !== 1) {
        return undefined;
    }

        for(i=0; i<sentence.length; i++) {
            if (letter === sentence[i]){
                result++;
            }

    }

    return result;
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

}

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    let nocasing = text.toLowerCase();
    let i = 0;
    let lettersInText = {};

    for(i=0; i<text.length; i++){
        let letter = nocasing[i];
        if (letter !== ' '){
            if(lettersInText[letter]){
                return false;
            }
            lettersInText[letter] = true;
        }
}

return true;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    let discountedPrice;

    if(age < 13 ) {
        return undefined;
    } else if ((age >= 13 && age <= 21) || age >= 60){
        discountedPrice = price*.8;
        let roundeddisc = discountedPrice.toFixed(2);
        return roundeddisc;
    } else if (age >= 22 && age <= 64){
        let roundedprice = price.toFixed(2);
        return roundedprice;
    }
    
}

function findHotCategories(items) {
    let i = 0;
    let hotCategories = [];
    for (i=0; i < items.length; i++){
        if (items[i].stocks === 0 && !hotCategories.includes(items[i].category)){
           hotCategories.push(items[i].category);
        }
    }

    return hotCategories;
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.


}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.
    let flyingVoter = [];

    for(let i=0; i < candidateA.length; i++){
        for(let j=0; j < candidateB.length; j++){
            if(candidateA[i] === candidateB[j] && !flyingVoter.includes(candidateA[i])){
                flyingVoter.push(candidateA[i]);
            }
        }
    }

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    return flyingVoter;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};