// Get post data
fetch("https://jsonplaceholder.typicode.com/posts")
	.then((response) => response.json())
	.then((posts) => showPosts(posts))

// Add new post
document.querySelector("#form-add-post").addEventListener('submit', (event) => {
	// 'preventDefault' function stops the default behavior of forms when submitting them which is refreshing the page.
	event.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1 // userId is only used since we are utilizing the jsonplaceholder API, and their backend requires a userId
		}),
		headers: {"Content-Type": "application/json"}
	})
	.then((response) => response.json())
	.then((result) => {
		console.log(result)
		alert("Post successfully created!")

		// After sending the request and getting a response, the input fields must be reset and be blank again
		document.querySelector("#txt-title").value = null
		document.querySelector("#txt-body").value = null
	})
})

// For showing posts in the div element
const showPosts = (posts) => {
	let post_entries = ''

	// Loops through entire 'posts' array and sets their properties (id, title, body) to their respective HTML elements
	posts.forEach((post) => {
		post_entries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>

				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	// By using 'innerHTML' we are able to convert the string format of 'post_entries' variable into HTML format
	document.querySelector('#div-post-entries').innerHTML = post_entries
}


// Edit post
// Transfers the id, title, and body to the input fields of the edit post form
const editPost = (post_id) => {
	let title = document.querySelector(`#post-title-${post_id}`).innerHTML
	let body = document.querySelector(`#post-body-${post_id}`).innerHTML

	document.querySelector("#txt-edit-id").value = post_id 
	document.querySelector("#txt-edit-title").value = title 
	document.querySelector("#txt-edit-body").value = body 

	// To enable the 'Update' button, we remove the 'disabled' attrible from it
	// Note: ALWAYS make sure that submit buttons are disabled when there is no data yet in any of the input fields of the form.
	document.querySelector("#btn-submit-update").removeAttribute('disabled')
}

// Update post
document.querySelector("#form-edit-post").addEventListener('submit', (event) => {
	event.preventDefault()

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {"Content-Type": "application/json"}
	})
	.then((response) => response.json())
	.then((result) => {
		console.log(result)
		alert("Post successfully updated!")

		document.querySelector("#txt-edit-id").value = null		
		document.querySelector("#txt-edit-title").value = null		
		document.querySelector("#txt-edit-body").value = null

		// Setting the 'disable' attribute back into the update button to prevent it from being clicked without any data.
		document.querySelectow	r("#btn-submit-update").setAttribute("disabled", true)		
	})
})

// ACTIVITY - Refactor the deletePost function to be able to delete an existing post from the API. The API should return a response containing the post that you just deleted. The output should be in the console logging the post that was just deleted.
const deletePost = (post_id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${post_id}`, {
		method: "DELETE"
	})
	.then((response) => response.json())
	.then((result) => {
		console.log(result)
		// Removes the post from the HTML document
		document.querySelector(`#post-${post_id}`).remove()
		alert('Post successfully deleted!')
	})
}

