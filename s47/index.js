// The 'document' represent the whole HTML page and the query selector targets a specific element based on its ID, class, or tag name
console.log(document.querySelector("#txt-first-name"));

/*document.getElementById("txt-first-name")
document.getElementByClassName("span-full-name")
document.getElementByTagName("input")*/

// [SECTION] Event Listeners
const txt_first_name = document.querySelector("#txt-first-name")
const txt_last_name = document.querySelector("#txt-last-name")
const span_full_name = document.querySelector(".span-full-name")


// Keyup -> once you type in the keyboard, it will run the event
txt_first_name.addEventListener('keyup', (event) => {
	span_full_name.innerHTML = txt_first_name.value
})

txt_last_name.addEventListener('keyup', (event) => {
	const firstName = txt_first_name.value
	const lastName = txt_last_name.value
	span_full_name.innerHTML = firstName + " " + lastName
})

txt_first_name.addEventListener('keyup', (event) => {
	// 
	console.log(event.target)
	console.log(event.target.value)
})

txt_last_name.addEventListener('keyup', (event) => {
	// 
	console.log(event.target)
	console.log(event.target.value)
})


