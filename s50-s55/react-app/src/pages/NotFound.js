import {useNavigate} from 'react-router-dom'
import {Fragment} from 'react'
import {Button} from 'react-bootstrap'

export default function NotFound(){

	const navigate = useNavigate()

 return(
 	<Fragment>
 	<h1>Page Not Found</h1>
 	<p>The page your are looking for cannot be found :(</p>
 	<Button variant="primary" onClick={() => navigate(-1)}>Go Back to Previous page</Button>
 	</Fragment>
 	)
}