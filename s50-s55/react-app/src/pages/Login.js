import {Form, Button, Row, Col} from 'react-bootstrap'
import {useState, useEffect,useContext} from 'react'
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){

const { user, setUser} = useContext(UserContext)
const [email, setEmail] = useState('')
const [password1, setPassword1] = useState('')
const [isActive, setIsActive] = useState(false)


function loginUser(event) {
	event.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
		method: 'POST',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
			email: email,
			password: password1
		})
	})
	.then(response=> response.json())
	.then(result => {
		if(typeof result.accessToken !== 'undefined'){
		localStorage.setItem('token', result.accessToken)
		retrieveUserDetails(result.accessToken)

			setEmail('')
			setPassword1('')

	/*// show an alert
	alert("Congratulations for logging in!!")*/

	Swal.fire({
		title: "Login Successful pareh",
		icon: "success",
		text: "Welcome to Zuitt!"
	})

	}
}).catch(error => {
			// Show an error alert
				Swal.fire({
					title: "Authentication failed my guy",
					icon: "error",
					text: "Kindly check your login credentials."
				})
		})

/*	// Set the email of the authentcated user in the local storage
	localStorage.setItem('email', email)

	setUser({
		email: localStorage.getItem('email')
	})

*/

}

const retrieveUserDetails = (token) => {
	fetch('http://localhost:3001/users/details', {
		headers: {
			Authorization: `Bearer ${token}`
		}
	})
	.then(response => response.json())
	.then(result => {
		setUser({
			id: result._id,
			isAdmin: result.isAdmin
		})
	})
}

// For form validation, we use the useEffect to track the valuse of the input firelds and run a validation condition everytime there is user input in those fields
useEffect(() => {
	if ((email !== '' && password1 !== '')){
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [email, password1])


	return(
		(user.id !== null) ?

			<Navigate to="/courses"/>

		:
		<Row>
		<Col>

		<Form onSubmit={(event) => loginUser(event)}>
		            <Form.Group controlId="userEmail">
		                <Form.Label>Email address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter email"
			                value = {email}
			                 onChange ={event => setEmail(event.target.value)}
			               
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="password1">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Password" 
			                value = {password1}
			                onChange ={event => setPassword1(event.target.value)}
			                required
		                />
		            </Form.Group>

		 


		            { isActive ? 
		                <Button variant="primary" type="submit" id="submitBtn">
		            	Submit
		            </Button>
		            :
		                <Button disabled variant="primary" type="submit" id="submitBtn">
		            	Submit
		            </Button>

		            }
		        
		        </Form>

		        </Col>
		        </Row>
		)
}