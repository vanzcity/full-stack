import {useState, useEffect, Fragment} from 'react'
import CourseCard from '../components/CourseCard.js'
import {Row, Col} from 'react-bootstrap'

export default function Courses(){

	const [courses, setCourses] = useState([])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(response => response.json())
		.then(result => {
			setCourses(result.map(course => {
				return(
					<CourseCard key={course._id} course={course}/>
					)
			}))
		})
	}, [])

	return (
	<Row>
		<Col>
		{courses}
		</Col>
		</Row>
		)
}