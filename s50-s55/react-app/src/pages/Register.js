import {Form, Button, Row, Col} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Navigate, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){

	const navigate = useNavigate()

const { user} = useContext(UserContext)
const [firstName, setFirstName] = useState("")
const [lastName, setLastName] = useState("")
const [mobileNumber, setMobileNumber] = useState('')
const [email, setEmail] = useState('')
const [password1, setPassword1] = useState('')
const [password2, setPassword2] = useState('')
const [isActive, setIsActive] = useState(false)


function registerUser(event) {
	event.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/users/check-email`,{
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			email: email
		})
	})
	.then(response => response.json())
	.then(result => {
		if(result === true){
			Swal.fire({
					title: "User Already exists!",
					icon: "error",
					text: "The email you provided already exists in the server! Provide another email for registration."
				})

		} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNumber,
						password: password1
					})
				})
				.then(response => response.json())
				.then(result => {
					if(typeof result !== "undefined"){
						// For clearing out the form
						setFirstName('')
						setLastName('')
						setMobileNumber('')
						setEmail('')
						setPassword1('')
						setPassword2('')

						Swal.fire({
							title: "Registration Successful!",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						navigate("/login")
					}
				}).catch(error => {
					Swal.fire({
						title: "Something went wrong :(",
						icon: "error",
						text: error.message
					})
				})
			}
		})
	}

// For form validation, we use the useEffect to track the valuse of the input firelds and run a validation condition everytime there is user input in those fields
useEffect(() => {
	if ((firstName !== '' && lastName !== '' && mobileNumber !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [firstName, lastName, mobileNumber, email, password1, password2])

	return(

		(user.id !== null) ?

			<Navigate to="/courses"/>

		:

		<Row>
		<Col>
		<Form onSubmit={(event) => registerUser(event)}>
					<h1>Register</h1>

						<Form.Group controlId="userFirstName">
			            <Form.Label>First Name</Form.Label>
			            <Form.Control 
			                type="text" 
			                placeholder="Enter your First Name" 
			                value={firstName}
			                onChange={event => setFirstName(event.target.value)}
			                required
			            />
			        </Form.Group>

			          <Form.Group controlId="userLastName">
			            <Form.Label>Last Name</Form.Label>
			            <Form.Control 
			                type="text" 
			                placeholder="Enter your Last Name" 
			                value={lastName}
			                onChange={event => setLastName(event.target.value)}
			                required
			            />
			        </Form.Group>

			           <Form.Group controlId="userMobileNumber">
			            <Form.Label>Mobile Number</Form.Label>
			            <Form.Control 
			                type="text" 
			                placeholder="Enter your Mobile Number" 
			                value={mobileNumber}
			                onChange={event => setMobileNumber(event.target.value)}
			                required
			            />
			        </Form.Group>


		            <Form.Group controlId="userEmail">
		                <Form.Label>Email address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter email"
			                value = {email}
			                 onChange ={event => setEmail(event.target.value)}
			                required
		                />

		                <Form.Text className="text-muted">
		                    We'll never share your email with anyone else.
		                </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="password1">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Password" 
			                value = {password1}
			                onChange ={event => setPassword1(event.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="password2">
		                <Form.Label>Verify Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Verify Password" 
			                 onChange ={event => setPassword2(event.target.value)}
			                required
		                />
		            </Form.Group>


		            { isActive ? 
		                <Button variant="primary" type="submit" id="submitBtn">
		            	Submit
		            </Button>
		            :
		                <Button disabled variant="primary" type="submit" id="submitBtn">
		            	Submit
		            </Button>

		            }
		        
		        </Form>
		        </Col>
		        </Row>
		)
}