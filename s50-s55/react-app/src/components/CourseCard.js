import {Card, Button} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types'


export default function CourseCard({course}){

/*	const {name, description, price} = course

	const [count, setCount] = useState(0);

	const [seats, setSeats] = useState(10);

	const [slotsAvailable, setSlotsAvailable] =useState(true);

	const [isOpen, setIsOpen] = useState(true);

	function enroll(){
		setCount(count + 1)
		setSeats (seats - 1)

		if(seats === 1){
		setSlotsAvailable(false);
	}
	};

	useEffect(() => {
		if (seats === 0){
			setIsOpen(false)
		}


	}, [seats])*/

	const {_id, name, description, price} = course;


	return (
		<Card className="my-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>

				{/*<Card.Subtitle>Enrollees:</Card.Subtitle>
				<Card.Text>{count}</Card.Text>
				<Card.Subtitle>Seats:</Card.Subtitle>
				<Card.Text>{seats}</Card.Text>
				<Card.Subtitle>Course is open:</Card.Subtitle>
				<Card.Text>{isOpen ? 'Yes' : 'No'}</Card.Text>
				<Button className="mb-3" variant="primary" onClick={enroll} disabled={!slotsAvailable}>Enroll</Button>*/}

				<Link className="btn btn-primary" to={`/courses/${_id}/view`}>View Course</Link>
			</Card.Body>
		</Card>
		)
};

CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

