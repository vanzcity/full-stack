import {Navbar, Container, Nav} from 'react-bootstrap'
import { Fragment, useState, useContext } from 'react'
import {Link, NavLink} from 'react-router-dom'
import UserContext from '../UserContext'

function AppNavbar(){

  const {user} = useContext(UserContext)

  return (
    <Navbar bg="light" expand="lg">
     <Container fluid>
        <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
              <Nav.Link as={NavLink} to="/">Home</Nav.Link>
              <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
              {(user.id !== null) ? 

              <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              :
               <Fragment>

                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                 <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                
              </Fragment>
            }

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default AppNavbar



