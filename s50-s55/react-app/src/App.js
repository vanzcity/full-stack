// Importables
import {Fragment, useState} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js'
import {Container} from 'react-bootstrap';
import CourseView from './components/CourseView.js';
import './App.css';
import {UserProvider} from './UserContext.js'


// Component function
function App() {

const [user, setUser] = useState({
  id: null,
  isAdmin: null
})

const unsetUser = () => {
  localStorage.clear()
}

  // The component function returns JSX syntax that serves as the UI of the component.
  // Note: JSX syntax may look like HTML but it is actually Javascript that is formatted to look like HTML and is not actually HTML. The benefit of JSX is the ability to easily integrate Javascript with HTML syntax.
  return (
    // When rendering multiple components, they must always be enclosed in a parent component/element.
    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
    {/* The 'Router' initializes that dynamic routing will be involved. */}
        <AppNavbar/>
        <Container>    
        <Routes> // The 'Routes' initialized the set of specific routes to be used
          // The 'Route' is the specific endpoint which will render a specific component
          <Route path="/" element={<Home/>}/>
          <Route path="/courses" element={<Courses/>}/>
          <Route path="/courses/:courseId/view" element={<CourseView/>}/>
          <Route path="/register" element={<Register/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/logout" element={<Logout/>}/> 

          {/*Activity for not found page*/}

          <Route path="*" element={<NotFound/>}/>
          {/*asterisk means any*/}
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
